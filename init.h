#pragma once
#include "windows.h"
#include <functional>

#include <GWCA/GWCA.h>

#include <GWCA/Utilities/Hooker.h>

#include <GWCA/Managers/RenderMgr.h>
#include <GWCA/Managers/ChatMgr.h>

DWORD __stdcall SafeThreadEntry(LPVOID mod);
DWORD __stdcall ThreadEntry(LPVOID);
static void GameLoop(IDirect3DDevice9* device);
static void CmdExit(const wchar_t* msg, int argc, wchar_t** argv);