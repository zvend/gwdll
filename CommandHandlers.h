#pragma once

#include <Windows.h>

#include "CommandStructs.h"

namespace GWCACom {

	class GWCAConnection;

	void __fastcall HandleHeartbeat(BaseRequest* req, GWCAConnection* comm);
	void __fastcall HandlePipeDisconnect(BaseRequest* req, GWCAConnection* comm);
	void __fastcall HandleMove(Move::Request* req, GWCAConnection* comm);
	void __fastcall HandleCloseServer(BaseRequest* req, GWCAConnection* comm);
}