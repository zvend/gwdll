#pragma once

#include <Windows.h>

#include <GWCA/Constants/Constants.h>

#include "CommandEnums.h"
#include <GWCA/GameEntities/Agent.h>
#include <GWCA/GameEntities/Item.h>
#include <GWCA/GameEntities/Skill.h>

using namespace GW;

namespace GWCACom {

	/*
	Each specific Request should have a default constructor.
	The constructor must set up the cmd field to the correct CMD value;

	Additional constructors that set up some or all fields are optionals
	and should be made for your convenience only. They also must set up
	the cmd field.

	A correspondent Reply should be present for every function that
	returns a value (e.g. non-void).
	*/

	struct BaseRequest {
		protected:
			BaseRequest(CMD _cmd) : cmd(_cmd) {}
		public:
			const CMD cmd;
	};

	struct BaseReply {
		protected:
			BaseReply() {}
	};

	struct Heartbeat {
		struct Request : BaseRequest {
			Request() : BaseRequest(HEARTBEAT) {}
		};
		typedef DWORD Reply;
	};

	struct Disconnect {
		struct Request : BaseRequest {
			Request() : BaseRequest(DISCONNECT_PIPE_CONNECTION) {}
		};
	};

	struct Move {
		struct Request : BaseRequest {
			Request(float _x, float _y, DWORD _platform_id)
				: BaseRequest(MOVE), x(_x), y(_y), platform_id(_platform_id) {}
			Request(float _x, float _y) : Request(_x, _y, 0) {}
			Request() : Request(0, 0, 0) {}
			float x;
			float y;
			DWORD platform_id;
		};
	};
}
