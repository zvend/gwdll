#include "pch.h"

#include <functional>
#include <Windows.h>

#include <GWCA/GWCA.h>
#include <GWCA/Utilities/Hooker.h>
#include <GWCA/Utilities/Scanner.h>

#include "GWCAServer.h"
#include "CommandEnums.h"
#include "CommandStructs.h"
#include "CommandHandlers.h"

FILE* console;
HMODULE dllmodule = 0;
GWCACom::CommServer* server = nullptr;

namespace Console {
	void open() {
		AllocConsole();
		freopen_s(&console, "CONOUT$", "w", stdout);
		freopen_s(&console, "CONOUT$", "w", stderr);
		SetConsoleTitleA("GwAPI Console");
	}

	void close() {
		fclose(console);
		HWND console_hwnd = GetConsoleWindow();
		FreeConsole();
		SendMessage(console_hwnd, WM_CLOSE, NULL, NULL);
	}
}

int run_server() {
	using namespace GWCACom;

	TCHAR buf[MAX_PATH];
	swprintf_s(buf, L"\\\\.\\pipe\\GWComm_%d", GetCurrentProcessId());
	printf("Pipe name: %S\n", buf);

	server = new CommServer(buf, kAmountofCommands);
	server->AddCommand<BaseRequest>(CMD::HEARTBEAT, HandleHeartbeat);
	server->AddCommand<BaseRequest>(CMD::DISCONNECT_PIPE_CONNECTION, HandlePipeDisconnect);
	server->AddCommand<BaseRequest>(CMD::UNLOAD_SERVER, HandleCloseServer);
	server->AddCommand<Move::Request>(CMD::MOVE, HandleMove);
	server->MainRoutine();

	return 0;
}

DWORD __stdcall MainLoop(LPVOID) {
	GW::HookBase::Initialize();

	Sleep(100);

	if (!GW::Initialize())
	{
		MessageBoxA(0, "Initializing failed, contact Developers about this.", "GwAPI Error", 0);
		FreeLibraryAndExitThread(dllmodule, EXIT_SUCCESS);
		return EXIT_SUCCESS;
	}
	Sleep(100);

	/*
	Main Loop
	*/
	run_server();
	/*
	End Main Loop
	*/

	GW::DisableHooks();

	while (GW::HookBase::GetInHookCount())
		Sleep(100);

	Sleep(100);

	GW::Terminate();

	Sleep(100);

	Console::close();
	FreeLibraryAndExitThread(dllmodule, EXIT_SUCCESS);
	return EXIT_SUCCESS;
}


DWORD __stdcall SafeThreadEntry(LPVOID module) {
	dllmodule = (HMODULE)module;
	__try {	
		
		return MainLoop(nullptr); 
	
	}
	__except (EXCEPTION_CONTINUE_SEARCH) { return EXIT_SUCCESS; }
}

DWORD WINAPI init(HMODULE hModule) 
{
	__try
	{
		Console::open();

		GW::Scanner::Initialize();

		DWORD** found = (DWORD**)GW::Scanner::Find("\xA3\x00\x00\x00\x00\xFF\x75\x0C\xC7\x05", "x????xxxxx", +1);

		if (!(found && *found))
		{
			MessageBoxA(nullptr, "No Logged In Character.", "GwAPI Error", 0);
			FreeLibraryAndExitThread(hModule, EXIT_SUCCESS);
		}

		DWORD* is_ingame = *found;

		while (*is_ingame == 0)
		{
			Sleep(100);
		}

		SafeThreadEntry(hModule);

		Console::close();
		FreeLibraryAndExitThread(hModule, EXIT_SUCCESS);
	}
	__except (EXCEPTION_CONTINUE_SEARCH) {}
	
	return 0;
}

BOOL WINAPI DllMain(_In_ HMODULE _HDllHandle, _In_ DWORD _Reason, _In_opt_ LPVOID _Reserved) 
{
		DisableThreadLibraryCalls(_HDllHandle);
		if (_Reason == DLL_PROCESS_ATTACH) {

			__try {
				HANDLE hThread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)init, _HDllHandle, 0, 0);
				if (hThread != NULL) CloseHandle(hThread);
			}
			__except (EXCEPTION_CONTINUE_SEARCH) {}

		}
		return TRUE;
}