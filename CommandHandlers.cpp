#include "pch.h"
#include "CommandHandlers.h"
#include "GWCAConnection.h"

#include <GWCA/GWCA.h>
#include <GWCA/Managers/AgentMgr.h>
#include <GWCA/Managers/GameThreadMgr.h>

using namespace GW;

void __fastcall GWCACom::HandleMove(Move::Request* req, GWCAConnection* comm) {
	GameThread::Enqueue([req]() {
		Agents::Move(req->x, req->y, req->platform_id);
	});
}

void __fastcall GWCACom::HandleHeartbeat(BaseRequest* req, GWCAConnection* comm) {
	printf("Heartbeat!\n");
	comm->Send<Heartbeat::Reply>(1);
}

void __fastcall GWCACom::HandlePipeDisconnect(BaseRequest* req, GWCAConnection* comm) {
	comm->Close();
}

void __fastcall GWCACom::HandleCloseServer(BaseRequest* req, GWCAConnection* comm) {
	printf("Close Server!\n");
	comm->parent()->~CommServer();
}