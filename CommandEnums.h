#pragma once

namespace GWCACom {
	enum CMD {
		HEARTBEAT,
		DISCONNECT_PIPE_CONNECTION,
		UNLOAD_SERVER,
		CLOSE_SERVER,
		MOVE,
		COMMANDS_END
	};

	const size_t kAmountofCommands = COMMANDS_END;

	enum Target {
		Self = -2,
		Current = -1
	};
}
